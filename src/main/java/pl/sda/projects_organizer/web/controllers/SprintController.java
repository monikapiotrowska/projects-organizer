package pl.sda.projects_organizer.web.controllers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.sda.projects_organizer.data.entity.Sprint;
import pl.sda.projects_organizer.data.entity.Task;
import pl.sda.projects_organizer.data.services.SprintService;
import pl.sda.projects_organizer.data.services.TaskService;

import java.time.LocalDate;
import java.util.*;

@Controller
public class SprintController {

    private final SprintService sprintService;
    private final TaskService taskService;

    public SprintController(SprintService sprintService, TaskService taskService) {
        this.sprintService = sprintService;
        this.taskService = taskService;
    }

    @GetMapping("/addNewSprint")
    public String showAddNewSprintForm(Model model) {
        model.addAttribute("sprint", new Sprint());
        return "addNewSprint";
    }

    @PostMapping("/addNewSprint")
    public String addNewSprint(@ModelAttribute("sprint") Sprint sprint, Model model) {
        if (sprint.getStoryPoints() == null) {
            //user did not populate the story points field
            model.addAttribute("invalidStoryPoints", "Please enter number of story points");
            return "addNewSprint";
        }
        if (sprint.getDateFrom() == null) {
            //user did not populate the date from
            model.addAttribute("invalidStartDate", "Invalid start date: mandatory field and can not be a date in the past");
            return "addNewSprint";
        }
        if (sprint.getDateFrom().isBefore(LocalDate.now())) {
            //user selected a date in the past as start date (not allowed by business logic)
            model.addAttribute("invalidStartDate", "Invalid start date: mandatory field and can not be a date in the past");
            return "addNewSprint";
        }
        if (sprint.getDateTo() == null) {
            //user did not populate the date to
            model.addAttribute("invalidFinishDate", "Invalid finish date");
            return "addNewSprint";
        }
        if (sprint.getDateTo().isBefore(sprint.getDateFrom())) {
            //user selected finish date earlier than start date (does not make sense)
            model.addAttribute("invalidFinishDate", "Invalid finish date: can not be earlier than start date");
            return "addNewSprint";
        }
        Sprint savedSprint = sprintService.createSprint(sprint);
        Long id = savedSprint.getId();
        return "redirect:/displaySprint/" + id;
        // https://www.baeldung.com/spring-redirect-and-forward
    }

    @GetMapping("/displaySprint/{id}")
    public String displaySprint(@PathVariable Long id, Model model) {
        Optional<Sprint> optionalSprint = sprintService.findById(id);
        Sprint sprint = new Sprint();
        if (optionalSprint.isPresent()) {
            sprint = optionalSprint.get();
        }
        model.addAttribute("sprint", sprint);
        return "sprintView";
    }

    @GetMapping("/editSprint/{id}")
    public String editSprint(@PathVariable Long id, Model model) {
        List<Task> availableTasks = taskService.findAllOpenTasks();
        List<Task> selectedTasks = new ArrayList<>();
        Sprint sprint = new Sprint();

        //validate if sprint id passed in url PathVariable exists:
        Optional<Sprint> optionalSprint = sprintService.findById(id);
        if (optionalSprint.isPresent()) {
            sprint = optionalSprint.get();
            //add all tasks assigned to this sprint into selectedTasks list
            selectedTasks.addAll(sprint.getTasks());
        }
        //pass objects to html page:
        model.addAttribute("availableTasks", availableTasks);
        model.addAttribute("selectedTasks", selectedTasks);
        model.addAttribute("sprint", sprint);
        return "editSprint";
    }

    /*Method receives ajax request from editSprint.html; the ajax request passes a JSON file containing
    the sprint ID and the ID's of the tasks that were chosen into this sprint ID; the method persists this;
    The method returns a String message as response to the ajax request; alternative option would be to
    return a JSON file as response, as per examples:
    https://www.mkyong.com/spring-mvc/spring-4-mvc-ajax-hello-world-example/
    https://www.mkyong.com/spring-boot/spring-boot-ajax-example/*/
    @PostMapping("/allocateTasks")
    public @ResponseBody
    String ajaxAllocateTasksToSprint(@RequestBody String receivedJsonString) {
        //this list will be populated with the task ID's received in JSON in @RequestBody
        List<Long> taskIds = new ArrayList<>();
        JSONParser jsonParser = new JSONParser();
/*        Parsing the contents of the JSON file
        https://www.tutorialspoint.com/how-to-read-parse-json-array-using-java*/
        try {
            JSONObject jsonObject = (JSONObject) jsonParser.parse(receivedJsonString);
            //get sprintId received in JSON file
            Long sprintID = Long.valueOf(jsonObject.get("sprintId").toString());
            //get taskID's received in JSON file
            JSONArray jsonArray = (JSONArray) jsonObject.get("taskIDs");
            //Iterating the contents of the array that holds task ID's
            for (Object o : jsonArray) {
                taskIds.add(Long.valueOf(o.toString()));
            }
            // logic for persistence
            // prepare list of tasks that should be assigned to the sprint
            List<Task> tasksToBeAssigned = new ArrayList<>();
            taskIds.forEach(taskId -> {
                tasksToBeAssigned.add(taskService.findTaskById(taskId));
            });
            // check which tasks are currently assigned to it in the Database
            List<Task> tasksCurrAssignedInDb = new ArrayList<>();
            List<Long> taskIDsToRemove = new ArrayList<>();
            Optional<Sprint> optionalSprint = sprintService.findById(sprintID); //validate if sprint ID is valid (wrap in Optional class)
            if (optionalSprint.isPresent()) {
                Sprint sprint = optionalSprint.get();
                tasksCurrAssignedInDb = sprint.getTasks(); //check which tasks are currently assigned to it in the Database
                // logic to update Task field isAssignedToSprint to TRUE
                for (Task task : tasksToBeAssigned) {
                    task.setIsAssignedToSprint(true);
                }
                ;
                // any task that was removed by user (is present in tasksCurrAssignedInDb but is not present in tasksToBeAssigned) should have its field isAssignedToSprint updated to false
                tasksCurrAssignedInDb.forEach(task -> taskIDsToRemove.add(task.getId()));
                for (Long tID : taskIDsToRemove) {
                    for (Task task : tasksToBeAssigned) {
                        if (task.getId().equals(tID)) {
                            taskIDsToRemove.remove(tID);
                            break;
                        };
                    }
                }

                // update field isAssignedToSprint to false for every task from the tasksToRemove List
                List<Task> tasksToRemove = taskService.findAllTasksById(taskIDsToRemove);
                tasksToRemove.forEach(task -> task.setIsAssignedToSprint(false));
                taskService.updateAll(tasksToRemove);

                // update sprint with tasks chosen by user
                sprint.setTasks(tasksToBeAssigned);
                sprintService.updateSprint(sprint, sprintID);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "tasks successfully allocated to sprint";
    }
}


