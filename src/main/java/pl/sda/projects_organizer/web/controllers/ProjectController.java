package pl.sda.projects_organizer.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.sda.projects_organizer.data.entity.Project;
import pl.sda.projects_organizer.data.entity.ProjectTeamMember;
import pl.sda.projects_organizer.data.entity.User;
import pl.sda.projects_organizer.data.services.ProjectService;
import pl.sda.projects_organizer.data.services.ProjectTeamMemberService;
import pl.sda.projects_organizer.data.services.UserService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@ControllerAdvice
public class ProjectController {

    private final UserService userService;
    private final ProjectTeamMemberService projectTeamMemberService;
    private final ProjectService projectService;
    private final HttpSession httpSession;

    @Autowired
    public ProjectController(UserService userService, ProjectTeamMemberService projectTeamMemberService, ProjectService projectService, HttpSession httpSession) {
        this.userService = userService;
        this.projectTeamMemberService = projectTeamMemberService;
        this.projectService = projectService;
        this.httpSession = httpSession;
    }

    @GetMapping("/addNewProject")
    public String showNewProjectForm(Model model){
        Project project = new Project();
        model.addAttribute("project", project);
        return "addNewProject";
    }

    @PostMapping("/addNewProject")
    public String addProject(@ModelAttribute("project") Project project){
        String name = project.getName();
        String description = project.getDescription();
        User admin = userService.getLoggedInUser();
        Project projectToSave = new Project(name, description);
        projectToSave.setAdmin(admin);
        projectService.createProject(projectToSave);
        return "projectView";
    }

    @GetMapping("/projectView/{projectId}")
    public String showProject(@PathVariable("projectId") Long id, Model model){
        Project project = projectService.findProjectById(id);
        httpSession.setAttribute("actualProject", project);
        model.addAttribute("project", project);
        return "projectView";
    }

    @GetMapping("/projects")
    public String showAllProjects (ModelMap map){
        map.addAttribute("projectsList", projectService.findAllProjects());
        return "projects";
    }

    @GetMapping("/userProjects")
    public String showUserProjects (ModelMap map){
        User loggedInUser = userService.getLoggedInUser();
        List<Project> projectsList = projectService.findProjectsByUser(loggedInUser);
        map.addAttribute("projectsList", projectsList);
        return "userProjects";
    }

    @GetMapping("/projectTeamMembers")
    public String showProjectTeamMembers (ModelMap map){
        Project actualProject = (Project) httpSession.getAttribute("actualProject");
        Long id = actualProject.getId();
        List<ProjectTeamMember> projectTeamMembersByProject = projectTeamMemberService.getProjectTeamMembersByProjectId(id);
        map.addAttribute("projectTeamMembers", projectTeamMembersByProject);
        return "projectTeamMembers";
    }

    @GetMapping("/editProject")
    public String editProject(Model model){
        Project project = (Project) httpSession.getAttribute("actualProject");
        model.addAttribute("project", project);
        return "editProject";
    }

    @PostMapping("/editProject")
    public String editProject(@ModelAttribute("project") Project project){
        Project projectFromDatabase = (Project) httpSession.getAttribute("actualProject");
        Long id = projectFromDatabase.getId();
        projectService.updateProject(project, id);
        return "projectView";
    }
}
