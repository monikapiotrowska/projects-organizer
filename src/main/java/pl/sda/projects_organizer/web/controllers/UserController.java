package pl.sda.projects_organizer.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;
import pl.sda.projects_organizer.data.entity.Project;
import pl.sda.projects_organizer.data.entity.ProjectTeamMember;
import pl.sda.projects_organizer.data.entity.User;
import pl.sda.projects_organizer.data.repo.ProjectRepository;
import pl.sda.projects_organizer.data.services.ProjectTeamMemberService;
import pl.sda.projects_organizer.data.services.UserService;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;

@Controller
public class UserController {

    private final UserService userService;
    private final ProjectRepository projectRepository;
    private final ProjectTeamMemberService projectTeamMemberService;
    private final HttpSession httpSession;

    @Autowired
    public UserController(UserService userService, ProjectRepository projectRepository, ProjectTeamMemberService projectTeamMemberService, HttpSession httpSession) {
        this.userService = userService;
        this.projectRepository = projectRepository;
        this.projectTeamMemberService = projectTeamMemberService;
        this.httpSession = httpSession;
    }

    @GetMapping("/registerNewUser")
    public String registerNewUser(Model model){
        model.addAttribute("user", new User());
        return "registerNewUser";
    }

    @PostMapping("/registerNewUser")
    public String processRegistrationForm(@ModelAttribute(value = "user") @Valid User user, Errors errors) {
        if (errors.hasErrors()){
            return "registerNewUser";
        }
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        userService.saveUser(user);
        return "redirect:/login";
    }


    @GetMapping("/login")
    public String login(Model model){
        model.addAttribute("user", new User());
        return "login";
    }

    @GetMapping("/users")
    public String showAll (ModelMap map){
        map.addAttribute("userList", userService.findAllUsers());
        return "users";
    }

    @GetMapping("/userPanel")
    public String userPanel(Model model){
        User user = userService.getLoggedInUser();
        model.addAttribute("loggedInUser", user);
        return "userPanel";
    }

    @GetMapping("/userView/{userId}")
    public String showUser(@PathVariable("userId") Long id, Model model){
        User user = userService.findUserById(id);
        httpSession.setAttribute("actualUser", user);
        model.addAttribute("user", user);
        return "userView";
    }

    @GetMapping("/editUser")
    public String editUser(Model model){
        User user = userService.getLoggedInUser();
        model.addAttribute("user", user);
        return "editUser";
    }

    @PostMapping("/editUser")
    public String updateUserDetails(@ModelAttribute("user") User user){
        Long id = userService.getLoggedInUser().getId();
        User userToUpdate = userService.getLoggedInUser();
        userToUpdate.setUserName(user.getUserName());
        userToUpdate.setEmail(user.getEmail());
        userToUpdate.setLogin(user.getLogin());
        userService.updateUserDetails(userToUpdate,id);
        return "redirect:/userPanel";
    }

    @GetMapping("/inviteUser/{userId}")
    public String inviteUser(@PathVariable("userId") Long id, Model model){
        User invitedUser = userService.findUserById(id);
        Project actualProject = (Project) httpSession.getAttribute("actualProject");
        model.addAttribute("invitedUser", invitedUser);
        model.addAttribute("actualProject", actualProject);
        ProjectTeamMember newMember = new ProjectTeamMember();
        newMember.setAddedDate(LocalDateTime.now());
        newMember.setProjectMember(invitedUser);
        newMember.setProject(actualProject);
        projectTeamMemberService.getProjectTeamMembersByProjectId(actualProject.getId()).add(newMember);
        projectRepository.save(actualProject);
        return "inviteUser";
    }

    @GetMapping("/usersToInvite")
    public String showUsers(ModelMap map){
        map.addAttribute("userList", userService.findUsersToInvite());
        return "usersToInvite";
    }
}

