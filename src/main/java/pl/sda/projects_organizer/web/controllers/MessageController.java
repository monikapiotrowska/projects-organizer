package pl.sda.projects_organizer.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import pl.sda.projects_organizer.data.entity.Message;
import pl.sda.projects_organizer.data.entity.User;
import pl.sda.projects_organizer.data.services.MessageService;
import pl.sda.projects_organizer.data.services.UserService;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

@Controller
public class MessageController {

    private final MessageService messageService;
    private final UserService userService;
    private final HttpSession httpSession;

    public MessageController(MessageService messageService, UserService userService, HttpSession httpSession) {
        this.messageService = messageService;
        this.userService = userService;
        this.httpSession = httpSession;
    }

    @GetMapping("/userMessages")
    public String getMessages(ModelMap map){
        map.addAttribute("userMessages", userService.getUserMessages());
        return "userMessages";
    }

    @GetMapping("/addMessage")
    public String writeMessage(Model model){
        model.addAttribute("message", new Message());
        return "addMessage";
    }

    @PostMapping("/addMessage")
    public String sendMessage(@ModelAttribute("newMessage") Message message){
        String newMessage = message.getMessage();
        Message messageToSave = new Message();
        User sender = userService.getLoggedInUser();
        User recipient = (User) httpSession.getAttribute("actualUser");
        messageToSave.setMessage(newMessage);
        messageToSave.setRecipient(recipient);
        messageToSave.setSender(sender);
        messageToSave.setSendedDate(LocalDateTime.now());
        userService.getLoggedInUser().getMessages().add(messageToSave);
        ((User) httpSession.getAttribute("actualUser")).getMessages().add(messageToSave);
        messageService.createMessage(messageToSave);
        return "sendedMessage";
    }

    @GetMapping("/messageView/{messageId}")
    public String showMessages(@PathVariable("messageId") Long id, Model model){
        Message message = messageService.getMessageById(id);
        User actualUser = userService.getLoggedInUser();
        model.addAttribute("message", message);
        model.addAttribute("actualUser", actualUser);
        return "messageView";
    }
}
