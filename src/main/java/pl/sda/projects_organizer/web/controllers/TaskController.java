package pl.sda.projects_organizer.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import pl.sda.projects_organizer.data.entity.Progress;
import pl.sda.projects_organizer.data.entity.Project;
import pl.sda.projects_organizer.data.entity.Task;
import pl.sda.projects_organizer.data.entity.User;
import pl.sda.projects_organizer.data.services.ProjectService;
import pl.sda.projects_organizer.data.services.TaskService;
import pl.sda.projects_organizer.data.services.UserService;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class TaskController {

    private final TaskService taskService;
    private final UserService userService;
    private final ProjectService projectService;
    private final HttpSession httpSession;

    @Autowired
    public TaskController(TaskService taskService, UserService userService, ProjectService projectService, HttpSession httpSession) {
        this.taskService = taskService;
        this.userService = userService;
        this.projectService = projectService;
        this.httpSession = httpSession;
    }

    @GetMapping("/addNewTask")
    public String addNewTask(Model model){
        model.addAttribute("task", new Task());
        return "addNewTask";
    }

    @PostMapping("/addNewTask")
    public String addNewTask(@ModelAttribute(value = "task") Task task, Model model) {
        User loggedInUser = userService.getLoggedInUser();
        task.setOwner(loggedInUser);
        Project actualProject = (Project) httpSession.getAttribute("actualProject");
        task.setProject(actualProject);
        task.setProgress(Progress.BACKLOG);
        taskService.createTask(task);
        return "taskView";
    }

    @GetMapping("/taskView/{taskId}")
    public String showTask(@PathVariable("taskId") Long taskId, Model model){
        Task task = taskService.findTaskById(taskId);
        model.addAttribute("task", task);
        return "taskView";
    }

    @GetMapping("/tasks/{projectId}")
    public String showAll (@PathVariable("projectId") Long projectId, ModelMap map){
        Project projectById = projectService.findProjectById(projectId);
        map.addAttribute("tasksList", projectById.getTasks());
        return "tasks";
    }

    @GetMapping("/userTasks")
    public String show (ModelMap map){
        User loggedInUser = userService.getLoggedInUser();
        List<Task> tasksByUserId = taskService.findTaskByUser(loggedInUser);
        map.addAttribute("tasksList", tasksByUserId);
        return "userTasks";
    }
}
