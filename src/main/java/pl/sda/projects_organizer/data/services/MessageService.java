package pl.sda.projects_organizer.data.services;

import org.springframework.stereotype.Component;
import pl.sda.projects_organizer.data.entity.Message;
import pl.sda.projects_organizer.data.repo.MessageRepository;

@Component
public class MessageService {

    private final MessageRepository messageRepository;

    public MessageService(MessageRepository messageRepository) {
        this.messageRepository = messageRepository;
    }

    public Message createMessage(Message message){
        return messageRepository.save(message);
    }

    public Message getMessageById(Long id){
        return messageRepository.findById(id).get();
    }
}
