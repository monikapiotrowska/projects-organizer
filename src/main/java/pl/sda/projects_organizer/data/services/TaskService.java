package pl.sda.projects_organizer.data.services;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;
import pl.sda.projects_organizer.data.entity.Progress;
import pl.sda.projects_organizer.data.entity.Task;
import pl.sda.projects_organizer.data.entity.User;
import pl.sda.projects_organizer.data.repo.TaskRepository;
import pl.sda.projects_organizer.data.repo.UserRepository;

import java.util.ArrayList;
import java.util.List;

@Component
public class TaskService {

    private final TaskRepository taskRepository;
    private final UserRepository userRepository;

    public TaskService(TaskRepository taskRepository, UserRepository userRepository) {
        this.taskRepository = taskRepository;
        this.userRepository = userRepository;
    }

    public Task createTask(Task task) {
        return taskRepository.save(task);
    }

    public void updateTask(Task task) {
        taskRepository.findById(task.getId()).map(
                taskFromDb -> {
                    taskFromDb.setName(task.getName());
                    taskFromDb.setDescription(task.getDescription());
                    taskFromDb.setProgress(task.getProgress());
                    taskFromDb.setPriority(task.getPriority());
                    taskFromDb.setStoryPoints(task.getStoryPoints());
                    taskFromDb.setOwner(task.getOwner());
                    taskFromDb.setIsAssignedToSprint(task.getIsAssignedToSprint());
                    return taskRepository.save(taskFromDb);
                }
        );
    }

    public void deleteTaskById(Long id) {
        taskRepository.deleteById(id);
    }

    public List<Task> findAllTasks() {
        return taskRepository.findAll();
    }

    public List<Task> findTaskByUser(User user) {
        return userRepository.findUserByLogin(user.getLogin()).getTasks();
    }

    public Task findTaskById(Long id) {
        return taskRepository.findById(id).isPresent() ? taskRepository.findById(id).get() : null;
    }

    public List<Task> findAllTasksById(List<Long> id) {
        return taskRepository.findAllById(id);
    }

    public List<Task> findAllOpenTasks() {
        return taskRepository.findAllByProgressIsNot(Progress.DONE);
    }

    public void updateAll(List<Task> tasks) {
        for (Task task : tasks) {
            updateTask(task);
        }

    }
}
