package pl.sda.projects_organizer.data.services;

import org.springframework.stereotype.Component;
import pl.sda.projects_organizer.data.entity.Project;
import pl.sda.projects_organizer.data.entity.ProjectTeamMember;
import pl.sda.projects_organizer.data.entity.User;
import pl.sda.projects_organizer.data.repo.ProjectRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
public class ProjectService {

    private final ProjectRepository projectRepository;
    private final UserService userService;
    private final ProjectTeamMemberService projectTeamMemberService;

    public ProjectService(ProjectRepository projectRepository, UserService userService, ProjectTeamMemberService projectTeamMemberService) {
        this.projectRepository = projectRepository;
        this.userService = userService;
        this.projectTeamMemberService = projectTeamMemberService;
    }

    public Project createProject(Project project){
        ProjectTeamMember projectTeamMember = new ProjectTeamMember();
        projectTeamMember.setProject(project);
        projectTeamMember.setAddedDate(LocalDateTime.now());
        User user = userService.getLoggedInUser();
        projectTeamMember.setProjectMember(user);
        List<ProjectTeamMember> membersList = new ArrayList<>();
        membersList.add(projectTeamMember);
        project.setProjectTeamMembers(membersList);
        return projectRepository.save(project);
    }

    public void updateProject(Project project, Long id){
        projectRepository.findById(id).map(
                projectFromDatabase -> {
                    projectFromDatabase.setName(project.getName());
                    projectFromDatabase.setDescription(project.getDescription());
                    return projectRepository.save(projectFromDatabase);
                }
        );
    }

    public List<Project> findAllProjects(){
        return projectRepository.findAll();
    }

    public List<Project> findProjectByUser(User loggedInUser) {
        return projectRepository.findByAdmin(loggedInUser);
    }

    public Project findProjectById(Long id){
        return projectRepository.findById(id).get();
    }

    public Project findProjectByName(String name){
        return projectRepository.findByName(name);
    }

    public List<Project> findProjectsByUser(User user){
        List<ProjectTeamMember> membersList = projectTeamMemberService.getProjectTeamMembersByUser(user);
        List<Project> projects = new ArrayList<>();
        for (ProjectTeamMember member: membersList) {
            projects.add(member.getProject());
        }
        return projects;
    }
}
