package pl.sda.projects_organizer.data.services;

import org.springframework.stereotype.Component;
import pl.sda.projects_organizer.data.entity.Sprint;
import pl.sda.projects_organizer.data.entity.Task;
import pl.sda.projects_organizer.data.repo.SprintRepository;

import java.util.List;
import java.util.Optional;

@Component
public class SprintService {

    private final SprintRepository sprintRepository;

    public SprintService(SprintRepository sprintRepository) {
        this.sprintRepository = sprintRepository;
    }

    public Sprint createSprint(Sprint sprint){
        return sprintRepository.save(sprint);
    }

    public void updateSprint(Sprint sprint, Long id){
        sprintRepository.findById(id).map(
                sprintFromDb -> {
                    sprintFromDb.setDateFrom(sprint.getDateFrom());
                    sprintFromDb.setDateTo(sprint.getDateTo());
                    sprintFromDb.setStoryPoints(sprint.getStoryPoints());
                    sprintFromDb.setTasks(sprint.getTasks());
                    return sprintRepository.save(sprintFromDb);
                }
        );
    }

    public void updateTasks(Long id, List<Task> tasks){
        sprintRepository.findById(id).map(
                sprintFromDb -> {
                    sprintFromDb.setTasks(tasks);
                    return sprintRepository.save(sprintFromDb);
                }
        );
    }


    public void deleteSprintById(Long id){
        sprintRepository.deleteById(id);
    }

    public Optional<Sprint> findById(Long id) {
        return sprintRepository.findById(id);
    }
}
