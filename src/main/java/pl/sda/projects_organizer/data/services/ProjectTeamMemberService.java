package pl.sda.projects_organizer.data.services;

import org.springframework.stereotype.Component;
import pl.sda.projects_organizer.data.entity.ProjectTeamMember;
import pl.sda.projects_organizer.data.entity.User;
import pl.sda.projects_organizer.data.repo.ProjectRepository;
import pl.sda.projects_organizer.data.repo.ProjectTeamMemberRepository;

import java.util.List;

@Component
public class ProjectTeamMemberService {

    private final ProjectTeamMemberRepository projectTeamMemberRepository;
    private final ProjectRepository projectRepository;

    public ProjectTeamMemberService(ProjectTeamMemberRepository projectTeamMemberRepository, ProjectRepository projectRepository) {
        this.projectTeamMemberRepository = projectTeamMemberRepository;
        this.projectRepository = projectRepository;
    }

    public List<ProjectTeamMember> getProjectTeamMembersByProjectId(Long id) {
        return projectRepository.findById(id).get().getProjectTeamMembers();
    }

    public List<ProjectTeamMember> getProjectTeamMembersByUser(User user){
        return projectTeamMemberRepository.getProjectTeamMembersByProjectMember(user);
    }
}
