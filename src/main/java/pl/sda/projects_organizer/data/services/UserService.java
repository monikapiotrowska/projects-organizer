package pl.sda.projects_organizer.data.services;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;
import pl.sda.projects_organizer.data.entity.Message;
import pl.sda.projects_organizer.data.entity.User;
import pl.sda.projects_organizer.data.repo.UserRepository;

import java.util.List;

@Component
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User saveUser(User user) {
        return userRepository.save(user);
    }

    public User findUserByLogin(String login){
        return userRepository.findUserByLogin(login);
    }

    public User findUserById(Long id) {
        return userRepository.findById(id).get();
    }

    public void deleteUserById(Long id){
        userRepository.deleteById(id);
    }

    public List<User> findAllUsers(){
        return userRepository.findAll();
    }

    public List<User> findUsersToInvite(){
        List<User> usersToInvite = userRepository.findAll();
        usersToInvite.remove(getLoggedInUser());
        return usersToInvite;
    }

    public User getLoggedInUser() {
        String login = SecurityContextHolder.getContext().getAuthentication().getName();
        return findUserByLogin(login);
    }

    public void updateUserDetails(User user, Long id){
        User userToUpdate = userRepository.findById(id).get();
        if (userToUpdate.getPassword().equals(new BCryptPasswordEncoder().encode(user.getPassword()))) {
            userRepository.findById(id).map(
                    userFromDatabase -> {
                        userFromDatabase.setLogin(user.getLogin());
                        userFromDatabase.setEmail(user.getEmail());
                        userFromDatabase.setUserName(user.getUserName());
                        return userRepository.save(userFromDatabase);
                    }
            );
        }
    }

    public List<Message> getUserMessages() {
        return getLoggedInUser().getMessages();
    }
}
