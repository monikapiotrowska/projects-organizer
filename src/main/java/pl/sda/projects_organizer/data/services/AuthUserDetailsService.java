package pl.sda.projects_organizer.data.services;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.sda.projects_organizer.data.entity.Role;
import pl.sda.projects_organizer.data.entity.User;

import java.util.HashSet;

@Service
public class AuthUserDetailsService implements UserDetailsService {
    private final UserService userService;

    public AuthUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    /**
     * metoda jest wywołana zawsze wtedy, kiedy spring rozpozna logowanie
     * i zostanie wpisane login i haslo. Naszym zadaniem jest sprawdzenie, czy lgoin
     * podany przez użytkownika istnieje w bazie
     * @param login
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        //sprawdzamy, czy użytkownik istnieje w naszej bazie.
        User myUser = userService.findUserByLogin(login);
        if (myUser == null) {
            //jeżeli uzytkownik nie istnieje, to pewnie później zostanie rzucony jakiś błąd
            //ale już na tym etapie możemy przerwać cały proces.
            throw new RuntimeException("No such user!");
        }
        //przgotowujemy sobie kolekcję, w której będziemy trzymac wszystkie role, które są przypisane
        // do naszego użytkownika
        HashSet<GrantedAuthority> roles = new HashSet<>();
        for (Role role : myUser.getRoles()) {
            //tworzymy sobie kolekcję z nazwami (wymóg springa) ROLE_nazwa roli np: ROLE_ADMIN
            roles.add(new SimpleGrantedAuthority("ROLE_"+role.getName()));
        }
        return new org.springframework.security.core.userdetails.User(login, myUser.getPassword(), roles);
    }
}
