package pl.sda.projects_organizer.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.projects_organizer.data.entity.ProjectTeamMember;
import pl.sda.projects_organizer.data.entity.User;

import java.util.List;

@Repository
public interface ProjectTeamMemberRepository extends JpaRepository<ProjectTeamMember,Long> {

    List<ProjectTeamMember> getProjectTeamMembersByProjectMember(User user);
}