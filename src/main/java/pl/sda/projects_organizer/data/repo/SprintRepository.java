package pl.sda.projects_organizer.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.projects_organizer.data.entity.Sprint;

import java.util.Optional;

@Repository
public interface SprintRepository extends JpaRepository<Sprint,Long> {

    public Optional<Sprint> findById(Long id);
}
