package pl.sda.projects_organizer.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.projects_organizer.data.entity.Message;

@Repository
public interface MessageRepository extends JpaRepository<Message,Long> {
}
