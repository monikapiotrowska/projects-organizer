package pl.sda.projects_organizer.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.projects_organizer.data.entity.Progress;
import pl.sda.projects_organizer.data.entity.Task;

import java.util.List;

@Repository
public interface TaskRepository extends JpaRepository<Task,Long> {
    List<Task> findAllByProgressIsNot(Progress progress);

}
