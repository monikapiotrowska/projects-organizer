package pl.sda.projects_organizer.data.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.sda.projects_organizer.data.entity.Project;
import pl.sda.projects_organizer.data.entity.User;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    List<Project> findByAdmin(User admin);

    Project findByName(String name);
}
