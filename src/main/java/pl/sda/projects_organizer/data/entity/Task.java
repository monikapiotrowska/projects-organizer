package pl.sda.projects_organizer.data.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String description;

    @Enumerated(EnumType.STRING)
    private Progress progress;
    private Integer storyPoints;
    private Integer priority;
    private Boolean isAssignedToSprint;

    @ManyToMany (mappedBy = "tasks")
    private List<Sprint> sprints;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private User owner;

    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;

    public Task(String name, String description, Integer storyPoints, Integer priority) {
        this.name = name;
        this.description = description;
        this.storyPoints = storyPoints;
        this.priority = priority;
    }
}
