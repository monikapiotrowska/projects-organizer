package pl.sda.projects_organizer.data.entity;

public enum Progress {

    BACKLOG,        // new tasks
    TO_DO,          // ?
    IN_PROGRESS,    // started
    QA,             // in testing
    DONE            // finished
}
