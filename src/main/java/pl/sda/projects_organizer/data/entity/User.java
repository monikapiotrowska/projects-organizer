package pl.sda.projects_organizer.data.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.Set;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String login;

    @NotEmpty
    private String password;

    @NotEmpty
    private String userName;

    @NotEmpty
    private String email;

    @OneToMany(mappedBy = "projectMember", cascade = CascadeType.PERSIST)
    private List<ProjectTeamMember> projectTeamMembers;

    @OneToMany(mappedBy = "owner", cascade = CascadeType.PERSIST)
    private List<Task> tasks;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name="user_id"),
    inverseJoinColumns = @JoinColumn(name="role_id"))
    private Set<Role> roles;

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "recipient", cascade = CascadeType.PERSIST)
    private List<Message> messages;

    //Constructor
    public User(String login, String password, String userName, String email) {
        this.login = login;
        this.password = password;
        this.userName = userName;
        this.email = email;
    }

    @Transient
    public void addToProjectTeamMember(ProjectTeamMember projectTeamMember) {
        projectTeamMember.setProjectMember(this);
        this.projectTeamMembers.add(projectTeamMember);
    }

    @Transient
    public void removeFromProjectsTeamMember(ProjectTeamMember projectTeamMember) {
        projectTeamMember.setProject(null);
        this.projectTeamMembers.remove(projectTeamMember);
    }

    @Transient
    public void addTask(Task task) {
        task.setOwner(this);
        this.tasks.add(task);
    }

    @Transient
    public void removeTask(Task task) {
        task.setOwner(null);
        this.tasks.remove(task);
    }
}
