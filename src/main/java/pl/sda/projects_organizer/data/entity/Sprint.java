package pl.sda.projects_organizer.data.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Sprint {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @DateTimeFormat(pattern = "yyyy-MM-dd") //required to Map HTML input date to LocalDate of Java Object
    private LocalDate dateFrom;

    @DateTimeFormat(pattern = "yyyy-MM-dd") //required to Map HTML input date to LocalDate of Java Object
    private LocalDate dateTo;

    private Integer storyPoints;

    // @ManyToMany with @JoinTable explained: https://www.concretepage.com/hibernate/hibernate-bidirectional-mapping-example-with-jointable-annotation
    @ManyToMany // Sprint owns this relationship hence the @JoinTable annotation below and not on Task entity
    @JoinTable(name = "tasks_in_sprints", joinColumns = @JoinColumn(name = "sprint_id"),
            inverseJoinColumns = @JoinColumn(name = "task_id"))
    private List<Task> tasks;

    public Sprint(LocalDate dateFrom, LocalDate dateTo, Integer storyPoints) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.storyPoints = storyPoints;
        this.tasks = new ArrayList<>();
    }

    @Transient
    public void addTasks(Task task) {
        List<Sprint> sprints = task.getSprints();
        sprints.add(this);
        task.setSprints(sprints);
        this.tasks.add(task);
    }

    @Transient
    public void removeTask(Task task) {
        List<Sprint> sprints = task.getSprints();
        sprints.remove(this);
        task.setSprints(sprints);
        this.tasks.remove(task);
    }


}
