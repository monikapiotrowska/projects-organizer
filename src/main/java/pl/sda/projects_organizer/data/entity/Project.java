package pl.sda.projects_organizer.data.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Entity
@Getter
@Setter
@RequiredArgsConstructor
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotEmpty
    private String name;

    @NotEmpty
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User admin;

    @OneToMany(mappedBy = "project", cascade = CascadeType.PERSIST, orphanRemoval = true)
    private List<ProjectTeamMember> projectTeamMembers;

    @OneToMany(mappedBy = "project", cascade = CascadeType.PERSIST)
    private List<Task> tasks;

    //Constructor
    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    /*@Transient
    public void addToProjectTeamMember(ProjectTeamMember projectTeamMember) {
        projectTeamMember.setProject(this);
        this.projectTeamMembers.add(projectTeamMember);
    }*/

    /*@Transient
    public void removeFromProjectsTeamMember(ProjectTeamMember projectTeamMember) {
        projectTeamMember.setProject(null);
        this.projectTeamMembers.remove(projectTeamMember);
    }*/

    @Transient
    public void addTask(Task task){
        task.setProject(this);
        this.tasks.add(task);
    }

    @Transient
    public void removeTask(Task task){
        task.setProject(null);
        this.tasks.remove(task);
    }
}
