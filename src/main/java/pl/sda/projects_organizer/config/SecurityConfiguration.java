package pl.sda.projects_organizer.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

// https://www.youtube.com/watch?v=3s2lSD50-JI

@EnableWebSecurity
@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        // https://docs.spring.io/spring-security/site/docs/current/reference/html/jc.html
        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                    .antMatchers("/home","/login", "/registerNewUser").permitAll()
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage("/login").permitAll().defaultSuccessUrl("/userPanel")
                    .and()
                .logout().logoutUrl("/logout").logoutSuccessUrl("/home");


        //https://docs.spring.io/spring-security/site/docs/current/reference/html/jc.html
//        httpSecurity
//                .logout()
//                    .logoutUrl("/perform_logout")
//                    .logoutSuccessUrl("/home");
    }

}
