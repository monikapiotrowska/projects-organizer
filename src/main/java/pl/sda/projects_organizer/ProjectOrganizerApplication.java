package pl.sda.projects_organizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class ProjectOrganizerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjectOrganizerApplication.class, args);
	}

}
